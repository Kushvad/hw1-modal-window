function Modal(props) {

    const {header, isOpen, isClose, text, actions} = props;

    if (!isOpen) return null;

    return (
        <div className= "modalBack" onClick={isClose}>
            <div className="modal" onClick={(e) => e.stopPropagation()}>
                <div className="header">
                    <h1 className="title">{header}</h1>                
                    <img className="closeButton" src="./img/cross.svg" alt="cross" onClick={isClose} />
                </div>
                <p className="text">{text}</p>
                <div className="buttonWrapper">

                    {actions.map((action, index) => (
                        <button className="actionButton" style={{ backgroundColor: action.backgroundColor }} key={index} onClick={action.onClick}>{action.label}</button>
                    ))}
                </div>
                
            </div>
        </div>
        
    )
}

export default Modal;
