function Button({text, backgroundColor, onClick}) {
       
    return (
        <button
            className="button"
            style={{ backgroundColor: backgroundColor }}
            onClick={onClick}
            >{text}
            </button>
    )
}

export default Button
