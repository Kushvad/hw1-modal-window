import { useState } from "react"
import Modal from "./components/Modal/Modal"
import Button from "./components/Button/Button";

function App() {

    const [firstModalOpen, setFirstModalOpen] = useState(false);
    const [secondModalOpen, setSecondModalOpen] = useState(false);

    const openFirstModal = () => {
        setFirstModalOpen(true);
    };

    const openSecondModal = () => {
        setSecondModalOpen(true);
    };

    const closeFirstModal = () => {
        setFirstModalOpen(false);
    };

    const closeSecondModal = () => {
        setSecondModalOpen(false);
    };

    return (
        <div style={{ margin: "150px auto", maxWidth: "450px", display: "flex", justifyContent: "center" }}>
            
            <Button text="Open first modal" backgroundColor="red" onClick={openFirstModal} />
            <Button text="Open second modal" backgroundColor="green" onClick={openSecondModal} />

            <Modal
                header="Do you want to delete this file?"
                isOpen={firstModalOpen}
                isClose={closeFirstModal}
                text="Once you delete this file, it won’t be possible to undo this action. 
Are you sure you want to delete it?"
                actions={[
                    {
                        label: 'Ok',
                        onClick: () => closeFirstModal()                       
                    },
                    {
                        label: 'Cancel',
                        onClick: () => closeFirstModal()
                    }
                ]}
            />

            <Modal
                header="Do you want to create new file?"
                isOpen={secondModalOpen}
                isClose={closeSecondModal}
                text="Are you sure,  you want to create it?"
                actions={[
                    {
                        label: 'Create',
                        onClick: () => closeSecondModal(),
                        backgroundColor: 'blue'
                    },
                    {
                        label: 'Cancel',
                        onClick: () => closeSecondModal(),
                        backgroundColor: 'gray'
                    }
                ]}
            />
            
        </div>
    );
}

export default App;
